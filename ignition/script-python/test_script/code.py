################################################################
################################################################
## DESC: For use in recording insights (once ~60secs)
## WARN: Modifying code may cause system to function incorrectly
## Copyright 2021 MHS Global, Inc. All rights reserved. 
################################################################
################################################################
import os
import system
import P_opc_data_api
import P_db_api
import P_system_utils

Module = 'MHS_Test_Module'
LOG = system.util.logger(Module)

def insert_log_table(module, level, message):
	query = "INSERT into equipments.logs (timestamp, module, level, message) VALUES(now(),?,?,?)"


	try:
		results = system.db.runPrepUpdate(query,[module, level, message], P_db_api.DB_CONNECTION)
		print('insert_log_table SUCCESS')
	except:
		log_string = 'insert_log_table: {0} : {1}'.format(sys.exc_info()[0],sys.exc_info()[1])
		LOG.error(log_string)
		print ('insert_log_table()::', log_string)
		

def read_timestamp_for_divert(pls_name):
	'''
	ns=1;s=[PS1-13SR]astDV_INSIGHTS[0].stData.liTS_ExtendSol
	'''
	path_to_read = '[PS1-13SR]astDV_INSIGHTS[0].stData.liTS_ExtendSol'

	result = P_opc_data_api.singleOPCRead(path_to_read)
	P_opc_data_api.ackTagCounter(pls_name, 'diDV_INSIGHTS_DATA_READ', 'diDV_INSIGHTS_DATA_READ_ACK')
	print('read_timestamp_for_divert => value ={}, timestamp='.format(result, result.timestamp))

def test_read_function(pls_name, function_name):
	plc_path = "[{}]".format(plc_name)				# [PS1-PLC]
	sensor_path = plc_path + SENSOR_PATH			# [PS1-PLC]stQM30VT_INSIGHTS
	
	device_id = P_opc_data_api.read_device_id(sensor_path)
	
	# Convert device id to DB data_source id
	datasource_id = P_db_api.get_data_source_id(str(device_id))
	if datasource_id is None:
		log_string= '{0}:: Cannot find data_source_id for device_id={1}. Skipping but acking'.format(Module, device_id)
		P_db_api.insert_log_table(Module,'WARNING',log_string)
		LOG.warn(log_string)
		# P_opc_data_api.ackTagCounter(plc_name, read_tag, read_ack_tag)
		return;
	
	function_name(datasource_id,sensor_path)
	return

def process(device_name):
	read_timestamp_for_divert('PS1-13SR')
	# Test log insertion
#	log_string= '{0}:: My message here device_id={1}'.format(Module, 12345)
#	insert_log_table(Module,'WARNING',log_string)

#	LOG.warn(log_string)
#	print(log_string)
