################################################################
################################################################
## DESC: For use in recording insights (once ~60secs)
## WARN: Modifying code may cause system to function incorrectly
## Copyright 2021 MHS Global, Inc. All rights reserved. 
################################################################
################################################################
import os
import system
import P_opc_data_api
import P_db_api
import P_system_utils

READ_INDICATOR_TAG = 'diVSE_INSIGHTS_DATA_READ'
ACK_INDICATOR_TAG = 'diVSE_INSIGHTS_DATA_READ_ACK'

SENSOR_PATH = 'stVSE_INSIGHTS'
DATAGROUP_DATA = '.astData'
DATAGROUP_INFO = '.stInfo'
DATAGROUP_DATA_FAILURE = '.diDataFailure'
DATAGROUP_STATS = '.stStats'
DATAGROUP_DeviceID = '.diDeviceID'
DATAGROUP_StructID = '.diStructID'
DATAGROUP_FAULTS = '.stFault'
DATASUBGROUP_IMPACT = '.stImpact'


# The number of measurements (reading) for each tag
# incapsulated in the high level data struct stVSE_INSIGHTS
NUM_MEASUREMENTS_IN_DATA_STRUCT = 6


# Logger:
Module = 'MHS_VSE'
LOG = system.util.logger(Module)

data_tag_to_metric_map = {
	'diStatus': P_db_api.DB_METRIC_OPERATION_CODE,
	P_opc_data_api.TAG_TIMESTAMP : P_db_api.DB_METRIC_TIMESTAMP,
	'rAccel_RMS_Freq': 'rms_freqency_acceleration',
	'rVelocity_RMS_Freq': 'rms_freqency_velocity',
	}

subgroup_data_tag_to_metric_map = {
	'diShoeID': 'shoe_id_impact_max',
	'diStatus': P_db_api.DB_METRIC_OPERATION_CODE,
	'rMax': 'impact_max',	#2230.12
	'rMean': 'impact_mean',
	'rMin': 'impact_min',	#1289.99
	'rSTD': 'impact_std',	#230.11
	P_opc_data_api.TAG_TIMESTAMP : P_db_api.DB_METRIC_TIMESTAMP,
	}

info_tag_to_metric_map =  {
	'iControlSW': 'control_sw', 	# control_sw
	P_opc_data_api.TAG_TIMESTAMP: P_db_api.DB_METRIC_TIMESTAMP,
	}
	
# 12-02-21: all values are 9,999
stats_tag_to_metric_map = {
	'iAnalogInput': '',
	'iAveragePower': '',
	'iControlTemp': '',
	'iDriveTemp': '',
	'iElapsedKwh': '',
	'iElapsedMwh': '',
	'iElapsedRunTimeHr': '',
	'iElapsedTimeHr': '',
	'iElapsedTimeMin': '',
	P_opc_data_api.TAG_TIMESTAMP: P_db_api.DB_METRIC_TIMESTAMP,
}

def read_datagroup_data(datasource_id, sensor_path):
	'''
	The enclosing folder of the tags is
		[PS1-PLC]stVSE_INSIGHTS.astData[1].stImpact
	'''
	for idx in range(NUM_MEASUREMENTS_IN_DATA_STRUCT):
		datagroup_path= [sensor_path + DATAGROUP_DATA + '[' + str(idx) + ']']
		
		P_system_utils.read_tags(datasource_id,
								 datagroup_path,
								 data_tag_to_metric_map)

		data_subgroup_paths = [sensor_path + DATAGROUP_DATA + '[' + str(idx) + ']' + DATASUBGROUP_IMPACT]
		P_system_utils.read_tags(datasource_id,
								 data_subgroup_paths,
								 subgroup_data_tag_to_metric_map)
				

			
def read_datagroup_info(datasource_id, sensor_path):
	datagroup_paths = [sensor_path + DATAGROUP_INFO]
	
	P_system_utils.read_tags(datasource_id,
							datagroup_paths,
							info_tag_to_metric_map)


def read_datagroup_stats(datasource_id, sensor_path):
	datagroup_paths = [sensor_path + '.' + DATAGROUP_STATS]

	P_system_utils.read_tags(datasource_id,
							datagroup_paths,
							stats_tag_to_metric_map)


def read_datagroup_faults(datasource_id, sensor_path):	
	pass


def test_function(plc_name):
		plc_path = "[{}]".format(plc_name)				# [PS1-13SR]
		sensor_path = plc_path + SENSOR_PATH			# [PS1-13SR]stVFD_INSIGHTS
	
		# Read device (sensor) id from PLC 
		device_id = P_opc_data_api.read_device_id(sensor_path)
		
		# Convert device id to DB data_source id
		datasource_id = P_db_api.get_data_source_id(str(device_id))
		if datasource_id is None:
			log_string= '{0}:: Cannot find data_source_id for device_id={1}. Skipping but acking'.format(Module, device_id)
			P_db_api.insert_log_table(Module,'WARNING',log_string)
			LOG.warn(log_string)
			# P_opc_data_api.ackTagCounter(plc_name, read_tag, read_ack_tag)
			return;
	
		#Function to test
		read_datagroup_info(datasource_id, sensor_path)

def process(plc_name):
	plc_path = '[{}]'.format(plc_name)				# [PS1-PLC]
	sensor_path = plc_path + SENSOR_PATH			# [PS1-PLC]stQM30VT_INSIGHTS
	
	P_system_utils.read_datagroups(plc_name,
									sensor_path,
									read_datagroup_data,
									read_datagroup_info,
									read_datagroup_stats,
									read_datagroup_faults)

	P_opc_data_api.ackTagCounter(plc_name, READ_INDICATOR_TAG, ACK_INDICATOR_TAG)
