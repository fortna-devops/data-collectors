################################################################
################################################################
## DESC: For use in recording insights (once ~60secs)
## WARN: Modifying code may cause system to function incorrectly
## Copyright 2021 MHS Global, Inc. All rights reserved.
################################################################
################################################################
import os
import system
import P_opc_data_api
import P_db_api
from datetime import datetime, timedelta


# Logger:
Module = 'MHS_System'
LOG = system.util.logger(Module)

def read_tags(db_datasource_id,
			  group_paths,			# list of datagroup or data-subgroup paths
			  tag_metric_map,
			  diStatus_tag_value=None):		# dictionary of tag-metric_names value pairs
	'''
	Read tags for multiple datagroups, or data subgroup,all of which
	have the same data_source id.
	'''
	tag_names = tag_metric_map.keys()		# tag_names is a list

	# Append tags to group/subgroup and create a flat list of tag paths
	tag_paths = []
	if len(group_paths) == 0:
		log_string= '{0}:: No tag paths provided for data_source_id={1}'.format(Module, db_datasource_id)
		P_db_api.insert_log_table(Module,'WARNING',log_string)
		LOG.warn(log_string)
		return
	elif len(group_paths) == 1:
		for tag in tag_names:
			tag_paths.append(group_paths[0] + '.' + tag)
	else:
		for idx in range(len(group_paths)):
			for tag in tag_names:
				tag_paths.append(group_paths[idx] + '.' + tag)
	
	# Read data from PLC
	measurements = P_opc_data_api.groupOPCRead(tag_paths)

	# Convert the results to a dictionary
	if len(group_paths) == 1:
		datagroup_dict = {}
		for indx in range(len(tag_names)):
			if measurements[0] is not None:
				metric_name = tag_metric_map[tag_names[indx]]
				datagroup_dict[metric_name] = measurements[indx]
	else:
		for idx in range(len(group_paths)):
			datagroup_dict = {}
			for indx in range(len(tag_names)):
				metric_name = tag_metric_map[tag_names[indx]]
				datagroup_dict[metric_name] = measurements[(idx * len(tag_names)) + indx]
			# Some data group/subgroup passed to this function may not have diStatus tag
			# so a special diStatus_tag_value is passed into be used.
			if diStatus_tag_value:
				datagroup_dict[P_db_api.DB_METRIC_OPERATION_CODE] = diStatus_tag_value
		
			P_db_api.insert_data(db_datasource_id, datagroup_dict)


def read_tags_DV(datasource_id,
			  group_paths,			# list of datagroup or data-subgroup paths
			  tag_metric_map):		# dictionary of tag-metric_names value pairs
	'''
	Read tags for multiple datagroups, or data subgroup,all of which
	have the same data_source id.
	'''
	tag_names = tag_metric_map.keys()		# tag_names is a list

	# Append tags to group/subgroup and create a flat list of tag paths
	tag_paths = []
	for idx in range(len(group_paths)):
		for tag in tag_names:
			tag_paths.append(group_paths[idx] + '.' + tag)

	# Read data from PLC. The disadvantage here is we make a
	# PLC read for every sensor.
	measurements = P_opc_data_api.groupOPCRead(tag_paths)

	# Convert the results and tags to a dictionary.
	# Convert also those tags which we will not store in the db; we use
	# their values to calculate values for other metrics which we will store.
	for idx in range(len(group_paths)):
		datagroup_dict = {}
		for indx in range(len(tag_names)):
			metric_name = tag_metric_map[tag_names[indx]]
			datagroup_dict[metric_name] = measurements[(idx * len(tag_names)) + indx]


		# Use value of retract_solenoid as its timestamp. All other metrics use
		# the value of liTS_ExtendSol/extend_solenoid as their timestamps. This means
		# there are two timestamps, necessitating two dictionaries
		retract_solenoid_dict = {
								'retract_solenoid': datagroup_dict['retract_solenoid'],
								P_db_api.DB_METRIC_TIMESTAMP: datagroup_dict['retract_solenoid'],
								P_db_api.DB_METRIC_OPERATION_CODE: P_db_api.DB_DEFAULT_OP_VALUE
								}
		P_db_api.insert_data(datasource_id, retract_solenoid_dict)


		# Use the liTS_ExtendSol/extend_solenoid value as the timestamp for all the other
		# tags (metrics) other than liTS_RetractSol/retract_solenoid
		datagroup_dict[P_db_api.DB_METRIC_TIMESTAMP] = datagroup_dict['extend_solenoid']
		datagroup_dict[P_db_api.DB_METRIC_OPERATION_CODE] = P_db_api.DB_DEFAULT_OP_VALUE

		# Calculate values for some metrics and add them to dictionary
		# so they will get added to the db
		# The values below are in MILLISECONDS
		datagroup_dict['td_moving_extend'] = datagroup_dict['liTS_MovingExtend'] - datagroup_dict['extend_solenoid']
		datagroup_dict['td_moving_retract'] = datagroup_dict['liTS_MovingRetract'] - datagroup_dict['retract_solenoid']
		datagroup_dict['td_extend_solenoid_px'] = datagroup_dict['liTS_ExtendSolPX'] - datagroup_dict['extend_solenoid']
		datagroup_dict['td_retract_solenoid_px'] = datagroup_dict['liTS_RetractSolPX'] - datagroup_dict['retract_solenoid']
		datagroup_dict['td_first_pin_extend_solenoid'] = datagroup_dict['aliTS_PinPrx_0'] - datagroup_dict['extend_solenoid'] # TODO xls says aliTS_PinPrx_1???
		datagroup_dict['td_last_pin_retract_solenoid'] = datagroup_dict['aliTS_PinPrx_2'] - datagroup_dict['retract_solenoid']
		# datagroup_dict['td_first_pin_extend_solenoid_px'] = datagroup_dict['aliTS_PinPrx_1'] - datagroup_dict['liTS_ExtendSolPX']
		# datagroup_dict['td_last_pin_retract_solenoid_px'] = datagroup_dict['aliTS_PinPrx_2'] - datagroup_dict['liTS_RetractSolPX']

		# Remove the metrics that are not going to be stored in db
		datagroup_dict.pop('aliTS_PinPrx_0')
		datagroup_dict.pop('aliTS_PinPrx_1')
		datagroup_dict.pop('aliTS_PinPrx_2')
		datagroup_dict.pop('aliTS_PinPrx_3')
		datagroup_dict.pop('liTS_ExtendSolPX')
		datagroup_dict.pop('liTS_MovingExtend')
		datagroup_dict.pop('liTS_MovingRetract')
		datagroup_dict.pop('liTS_RetractSolPX')

		P_db_api.insert_data(datasource_id, datagroup_dict)


def read_datagroups(plc_name,
					sensor_path,
					read_datagroup_data_func,
					read_datagroup_info_func,
					read_datagroup_stats_func,
					read_datagroup_faults_func):

	# Read device (sensor) id from PLC
	device_id = P_opc_data_api.read_device_id(sensor_path)

	# Convert device id to DB data_source id
	datasource_id = P_db_api.get_data_source_id(str(device_id))
	if datasource_id is None:
		log_string= '{0}:: No ds_id for device_id={1} tag_path={2}. Skipping but acking'.format(Module, device_id, sensor_path)
		P_db_api.insert_log_table(Module,'WARNING',log_string)
		LOG.warn(log_string)
		# P_opc_data_api.ackTagCounter(plc_name, read_tag, read_ack_tag)
		return;

	struct_id = P_opc_data_api.read_struct_id(sensor_path)
	if struct_id == 0:
		read_datagroup_data_func(datasource_id, sensor_path)
		read_datagroup_info_func(datasource_id, sensor_path)
		read_datagroup_stats_func(datasource_id, sensor_path)
		read_datagroup_faults_func(datasource_id, sensor_path)
	elif struct_id == 1:
		read_datagroup_info_func(datasource_id, sensor_path)
	elif struct_id == 2:
		read_datagroup_data_func(datasource_id, sensor_path)
	elif struct_id == 3:
		read_datagroup_stats_func(datasource_id, sensor_path)
	elif struct_id == 4:
		read_datagroup_faults_func(datasource_id, sensor_path)
	else:
 		log_string='Invalid diStructID={} plc={} device={}'.format(struct_id, plc_name, str(device_id))
		LOG.warn(log_string)
