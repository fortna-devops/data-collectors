################################################################
################################################################
## DESC: For use in recording insights (once ~60secs)
## WARN: Modifying code may cause system to function incorrectly
## Copyright 2021 MHS Global, Inc. All rights reserved. 
################################################################
################################################################

import os
import system


DEFAULT_DEVICE = "test_reveal" # opcua device name
#read_data_failure_tagname = "diDataFailure"
DATA_FAILURE = "diDataFailure"
SENSOR_ID = "diSensorID"
SORTER_ID = "diSorterID"
STRUCTID = "diStructID"
DEVICE_ID = "diDeviceID"
SENSOR_INFO = 'stInfo'
SENSOR_DATA = "stData"
SENSOR_STATS = "stStats"
SENSOR_FAULT = "stFault"
DIVERT_READ_SIZE ="diDV_INSIGHTS_DATA_SAMPLE_SIZE"

DEFAULT_OPC_SERVER = "Ignition OPC UA Server"

#GROUP_TAGS = [DATA_FAILURE,DEVICE_ID,STRUCTID]  # still needed??
DEFAULT_OP_VALUE = 0
TAG_OPERATION_STATUS = 'diSorterOpStatus'
TAG_TIMESTAMP = "liTimeStamp"

def packagedOPCRead(*args):
	'''
	IMPORTANT: the paths of the PLC tags to be fed into this
	function should be the "Item Path" that is obtained
	when you right clicked on the tag name in the OPC browser of Ignition
	'''
	# Flatten 2-level lists:
	tags = [tag for tags in args for tag in tags]
	values = [value.value for value in system.opc.readValues(DEFAULT_OPC_SERVER, tags)]
	
	# Extract values from flattened values and re-build the 2-level list
	lists = []
	pos = 0
	for i in range(len(args)):
		length = len(args[i])
		lists.append(values[pos:(pos+length)])
		pos += length
	return lists

def singleOPCRead(tag):
	'''
	tag: a string specifying the tag path
	'''
	value = system.opc.readValue(DEFAULT_OPC_SERVER,tag)
	return value.getValue()
	
def singleOPCWrite(tag,val):		
		returnQuality = system.opc.writeValue(DEFAULT_OPC_SERVER, tag, val)
		return returnQuality
		
def groupOPCRead(tags):
	#values = system.opc.readValues(DEFAULT_OPC_SERVER, tags)
	values = [value.value for value in system.opc.readValues(DEFAULT_OPC_SERVER, tags)]
	return values
	

def ackTagCounter(device_name, read_tag_name, write_tag_name):
	'''
	acknowledge to the PLC tag counter 
	'''	
		
	read_opc_path = "[{}]{}".format(device_name, read_tag_name)
	write_opc_path = "[{}]{}".format(device_name, write_tag_name)
	
	currentValue = singleOPCRead(read_opc_path)
	returnQuality = singleOPCWrite(write_opc_path, currentValue)

# [default]PS1-13SR/stQM30VT_INSIGHTS/diDeviceID.value
def read_device_id(sensor_path):
	device_id_path=sensor_path + '.' + 'diDeviceID'
	result = singleOPCRead(device_id_path)
	return result


# [default]PS1-13SR/stQM30VT_INSIGHTS/diStructID.value
def read_struct_id(sensor_path):
	'''
	diStructID is a misnomer that indicates which datagroup
	we should read
	'''
	struct_id_path=sensor_path + '.' + 'diStructID'
	result = singleOPCRead(struct_id_path)
	return result


# [default]PS1-13SR/stQM30VT_INSIGHTS/diStructID.value
def read_data_failure(sensor_path):
	'''
	diDataFailure is a tag that indicates whether ????
	'''
	data_failure_path=sensor_path + '.' + 'diDataFailure'
	result = singleOPCRead(data_failure_path)
	return result
