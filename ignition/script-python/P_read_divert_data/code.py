###############################################################
################################################################
## DESC: For use in recording insights (once ~60secs)
## WARN: Modifying code may cause system to function incorrectly
## Copyright 2021 MHS Global, Inc. All rights reserved. 
################################################################
################################################################
import os
import system
import time
import P_opc_data_api
import P_db_api
import P_system_utils


READ_INDICATOR_TAG = 'diDV_INSIGHTS_DATA_READ'
ACK_INDICATOR_TAG = 'diDV_INSIGHTS_DATA_READ_ACK'

SENSOR_PATH = 'astDV_INSIGHTS'
DATAGROUP_DATA = '.stData'
DATAGROUP_INFO = '.stInfo'
DATAGROUP_STATS = '.stStats'
DATAGROUP_DATA_FAILURE = '.diDataFailure'

TAG_DIVERT_SAMPLE_SIZE = 'diDV_INSIGHTS_DATA_SAMPLE_SIZE'

## The number of measurements (reading) for each tag
## incapsulated in the high level data struct stQM30VT_INSIGHTS
#NUM_MEASUREMENTS_IN_DATA_STRUCT = 6

# Logger:
Module = "MHS_DIVERT"
LOG = system.util.logger(Module)

info_tag_to_metric_map =  {
	'iControlSW': 'control_sw',		#0; TODO - no metric assigned
	 P_opc_data_api.TAG_TIMESTAMP : P_db_api.DB_METRIC_TIMESTAMP,  #0
#	 P_opc_data_api.TAG_OPERATION_STATUS: 'operational_status'
}

stats_tag_to_metric_map = {
	'diCycleTotalizer': 'cycle_total',				#57170
	'diDutyCycle_1min': 'duty_cycles_1min',		#42
	'diExtendAlarmRedTotalizer':'extend_red_alarm_total',			#0
	'diExtendAlarmYellowTotalizer':'extend_yellow_alarm_total',	#0
	'diPinStrikesTotalizer': 'pin_strikes_total',					#563821
	'diRetractAlarmRedTotalizer':'retract_red_alarm_total',		#0
	'diRetractAlarmYellowTotalizer':'retract_yellow_alarm_total',	#0
	 P_opc_data_api.TAG_TIMESTAMP : P_db_api.DB_METRIC_TIMESTAMP,
}

data_tag_to_metric_map = {
	'aliTS_PinPrx[0]': 'aliTS_PinPrx_0',   	# Do not store in db
	'aliTS_PinPrx[1]': 'aliTS_PinPrx_1',		# Do not store in db
	'aliTS_PinPrx[2]': 'aliTS_PinPrx_2',		# Do not store in db
	'aliTS_PinPrx[3]': 'aliTS_PinPrx_3',		# Do not store in db
	'diAirPressure': 'air_pressure',		#92
	'diShoeCount': 'shoe_count',			#8
	'diShoeID': 'shoe_id',					#569
	'diSorterSpeed': 'sorter_speed',		#2674
	'liTS_ExtendSol': 'extend_solenoid',
	'liTS_ExtendSolPX': 'liTS_ExtendSolPX',		# Do not store in db
	'liTS_MovingExtend': 'liTS_MovingExtend',		# Do not store in db
	'liTS_MovingRetract': 'liTS_MovingRetract',	# Do not store in db	
	'liTS_RetractSolPX': 'liTS_RetractSolPX',		# Do not store in db
	'liTS_RetractSol': 'retract_solenoid',
	
	# Tags below DO NOT EXIST anymore (their metric names can be re-purposed
	#
#	'diDivertMaxCurrent': 'divert_max_current',
#	'diRetractMaxCurrent': 'retract_max_current',
#	'diImpact_Cycle_Avg': 'impact_cycle_avg',
#	'diImpact_Cycle_Peak': 'impact_cycle_peak',
#	'diImpact_Ext_Peak': 'impact_ext_peak',
#	'diImpact_Ret_Peak': 'impact_ret_peak',
#	'': 'td_moving_extend',
#	'': 'td_extend_solenoid_px',
#	'': 'td_moving_retract',
#	'': 'td_retract_solenoid_px',
#	'': 'td_first_pin_extend_solenoid',
#	'': 'td_last_pin_retract_solenoid',
#	'': 'td_first_pin_extend_solenoid_px',
#	'': 'td_last_pin_retract_solenoid_px',
#	'': P_db_api.DB_TIMESTAMP,
#	 P_opc_data_api.TAG_OPERATION_STATUS: 'operational_status'
	 # timestamp should be same as liTS_MovingExtend ?
	 #P_opc_data_api.TAG_TIMESTAMP : 'ff'
}


def read_sample_size(plc_path):
	'''
	tag path = ns=1;s=[PS1-13SR]diDV_INSIGHTS_DATA_SAMPLE_SIZE
	'''
	group_path = plc_path + TAG_DIVERT_SAMPLE_SIZE
	
	sample_size = P_opc_data_api.singleOPCRead(group_path)
	return sample_size


def  read_datagroup_data(datasource_id, sensor_path):
	'''
	The enclosing folder of the tags is
		[PS1-13SR]astDV_INSIGHTS[100].stData
	'''
	datagroup_paths= [sensor_path + DATAGROUP_DATA]	# [PS1-13SR]astDV_INSIGHTS[idx].astData
	P_system_utils.read_tags_DV(datasource_id,
							 datagroup_paths,
							 data_tag_to_metric_map)


def read_datagroup_info(datasource_id, sensor_path):
	pass
# 12-02-21: there is not metric control_sw associated 
# with divert data sources in the db. Turning on below
# code will produce warning.

#	datagroup_paths= [sensor_path + DATAGROUP_INFO]
#	P_system_utils.read_tags(datasource_id,
#							datagroup_paths,
#							info_tag_to_metric_map)


def read_datagroup_stats(datasource_id, sensor_path):	
	datagroup_paths= [sensor_path + DATAGROUP_STATS]
	P_system_utils.read_tags(datasource_id,
							datagroup_paths,
							stats_tag_to_metric_map)


def read_datagroup_faults(datasource_id, sensor_path):	
	pass


def read_multi_sensors(plc_name):
	'''
	Read data from multiple sensors in one operation.
	'''

	plc_path = '[{}]'.format(plc_name)				# [PS1-13SR]
	num_sensors = read_sample_size(plc_path)
	
	if (num_sensors is None) or (num_sensors == 0):
		return
	
	sensor_path_stem = plc_path + SENSOR_PATH			# [PS1-13SR]astDV_INSIGHTS
	
	for idx in range(num_sensors):		
		sensor_path= sensor_path_stem + '[' + str(idx) + ']'   # [PS1-13SR]astDV_INSIGHTS[idx]
		P_system_utils.read_datagroups(plc_name,
									sensor_path,
									read_datagroup_data,
									read_datagroup_info,
									read_datagroup_stats,
									read_datagroup_faults)

	P_opc_data_api.ackTagCounter(plc_name, READ_INDICATOR_TAG, ACK_INDICATOR_TAG)


def test_function(plc_name):
	plc_path = "[{}]".format(plc_name)				# [PS1-13SR]
	sensor_path = plc_path + SENSOR_PATH + '[0]'			# [PS1-13SR]astDV_INSIGHS[0]

	# Read device (sensor) id from PLC 
	device_id = P_opc_data_api.read_device_id(sensor_path)
	
	# Convert device id to DB data_source id
	datasource_id = P_db_api.get_data_source_id(str(device_id))
	if datasource_id is None:
		log_string= '{0}:: Cannot find data_source_id for device_id={1}. Skipping but acking'.format(Module, device_id)
		P_db_api.insert_log_table(Module,'WARNING',log_string)
		LOG.warn(log_string)
		return;

	#Function to test
	read_datagroup_info(datasource_id, sensor_path)
#	read_datagroup_stats(datasource_id, sensor_path)

def process(plc_name):
	read_multi_sensors(plc_name)
	P_opc_data_api.ackTagCounter(plc_name, READ_INDICATOR_TAG, ACK_INDICATOR_TAG)
