################################################################
################################################################
## DESC: For use in recording insights (once ~60secs)
## WARN: Modifying code may cause system to function incorrectly
## Copyright 2021 MHS Global, Inc. All rights reserved. 
################################################################
################################################################
import os
import system
import P_opc_data_api
import P_db_api
#import P_sensorid_dbsensorid
import P_system_utils

################ Constants ################
#ns=1;s=[PS1-13SR]stQM30VT_INSIGHTS.astData[0].iTemperature

READ_INDICATOR_TAG = 'diQM30VT_INSIGHTS_DATA_READ'
ACK_INDICATOR_TAG = 'diQM30VT_INSIGHTS_DATA_READ_ACK'

SENSOR_PATH = 'stQM30VT_INSIGHTS'
DATAGROUP_DATA = '.astData'
DATAGROUP_INFO = '.stInfo'
DATAGROUP_STATS = '.stStats'
DATAGROUP_DATA_FAILURE = '.diDataFailure'

# Examples:
# stQM30VT_INSIGHTS.astData[0].rX_AXIS_RMSVelocity
# stQM30VT_INSIGHTS.astData[0].rY_AXIS_RMSVelocity

# The number of measurements (reading) for each tag
# incapsulated in the high level data struct stQM30VT_INSIGHTS
NUM_MEASUREMENTS_IN_DATA_STRUCT = 6

# Logger:
Module = 'MHS_BANNER'
LOG = system.util.logger(Module)

data_tag_to_metric_map = {
	'diStatus': P_db_api.DB_METRIC_OPERATION_CODE,
	P_opc_data_api.TAG_TIMESTAMP : P_db_api.DB_METRIC_TIMESTAMP,
	'rTemperature': 'temperature',
	#'rTemperature_Imperial': 'temperature_imperial',
	'rX_AXIS_CrestFactor': 'crest_factor_x',		#3.82
	'rZ_AXIS_CrestFactor': 'crest_factor_z',		#3.99
	'rX_AXIS_HighFreqRMSAccel': 'hf_rms_acceleration_x',	#0.09
	'rZ_AXIS_HighFreqRMSAccel': 'hf_rms_acceleration_z',	#0.06

	'rX_AXIS_Kurtosis': 'kurtosis_x',			#2.9
	'rZ_AXIS_Kurtosis': 'kurtosis_z',			#2.9
	'rX_AXIS_PeakVelocity': 'peak_velocity_x',	#3.73
	'rZ_AXIS_PeakVelocity': 'peak_velocity_z',	#1.5
	#'rX_AXIS_PeakVelocity_Imperial': 'peak_velocity_x_imperial', #value always 0, commented out to prevent error msg
	#'rZ_AXIS_PeakVelocity_Imperial': 'peak_velocity_z_imperial', #value always 0, commented out to prevent error msg
	'rX_AXIS_PeakVelocityCompFreq': 'peak_velocity_comp_freq_x',  #26.8
	'rZ_AXIS_PeakVelocityCompFreq': 'peak_velocity_comp_freq_z',	#12.2
	'rX_AXIS_RMSAcceleration': 'rms_acceleration_x',	#0
	'rZ_AXIS_RMSAcceleration': 'rms_acceleration_z',	#0
	'rX_AXIS_RMSPeakAcceleration': 'rms_peak_acceleration_x',  #0.36
	'rZ_AXIS_RMSPeakAcceleration': 'rms_peak_acceleration_z',	#0.25

	'rX_AXIS_RMSVelocity': 'rms_velocity_x',	#2.43
	'rZ_AXIS_RMSVelocity': 'rms_velocity_z',	#1.09

	#'rX_AXIS_RMSVelocity_Imperial': 'rms_velocity_x_imperial',
	#'rZ_AXIS_RMSVelocity_Imperial': 'rms_velocity_z_imperial'
}

info_tag_to_metric_map = {
	'diModel_Number': 'model_number',
	'diSerial_Number': 'serial_number',
	'iEEPROM_Part_Number': 'eeprom_part_num',
	'iEEPROM_Version_Lower': 'eeprom_version',
	'iEEPROM_Version_Upper': 'eeprom_version_upper',
	'iFirmware_Engineering': 'firmware_engineering',
	'iFirmware_Version_Lower': 'firmware_version',
	'iFirmware_Version_Upper': 'firmware_version_upper',
	#'iSampleID': 'iSampleID',
	'liTimeStamp': 'timestamp',
	#'siCounter': 'siCounter',
	'siFirmware_Part_Number': 'firmware_part_num',
	'siProduction_Date': 'production_date'
	#'siRegRead': 'siRegRead'
	}

stats_tag_to_metric_map = {
	P_opc_data_api.TAG_TIMESTAMP: P_db_api.DB_METRIC_TIMESTAMP,
}

def  read_datagroup_data(datasource_id, sensor_path):
	'''
	[PS1-13SR]stQM30VT_INSIGHTS.astData[0].iTemperature
	...
	[PS1-13SR]stQM30VT_INSIGHTS.astData[0].liTimeStamp

	'''
	datagroup_paths=[]
	for idx in range(NUM_MEASUREMENTS_IN_DATA_STRUCT):
		datagroup_paths.append(sensor_path + DATAGROUP_DATA + '[' + str(idx) + ']')
	
	P_system_utils.read_tags(datasource_id,
							 datagroup_paths,
							 data_tag_to_metric_map)

		
def read_datagroup_info(datasource_id, sensor_path):	
	datagroup_paths= [sensor_path + DATAGROUP_INFO]

	P_system_utils.read_tags(datasource_id,
							datagroup_paths,
							info_tag_to_metric_map)

def read_datagroup_stats(datasource_id, sensor_path):	
	pass


def read_datagroup_faults(datasource_id, sensor_path):	
	
# TODO: currently not doing anything with data_failure
# data_failure = P_opc_data_api.read_data_failure(sensor_path)

	pass

def test_function(plc_name):
	plc_path = "[{}]".format(plc_name)				# [PS1-13SR]
	sensor_path = plc_path + SENSOR_PATH			# [PS1-13SR]stVFD_INSIGHTS

	# Read device (sensor) id from PLC 
	device_id = P_opc_data_api.read_device_id(sensor_path)
	
	# Convert device id to DB data_source id
	datasource_id = P_db_api.get_data_source_id(str(device_id))
	if datasource_id is None:
		log_string= '{0}:: Cannot find data_source_id for device_id={1}. Skipping but acking'.format(Module, device_id)
		P_db_api.insert_log_table(Module,'WARNING',log_string)
		LOG.warn(log_string)
		# P_opc_data_api.ackTagCounter(plc_name, read_tag, read_ack_tag)
		return;

	#Function to test
	read_datagroup_info(datasource_id, sensor_path)

def process(plc_name):
	plc_path = '[{}]'.format(plc_name)				# [PS1-PLC]
	sensor_path = plc_path + SENSOR_PATH			# [PS1-PLC]stQM30VT_INSIGHTS

	P_system_utils.read_datagroups(plc_name,
	 								sensor_path,
	 								read_datagroup_data,
	 								read_datagroup_info,
	 								read_datagroup_stats,
	 								read_datagroup_faults)

	P_opc_data_api.ackTagCounter(plc_name, READ_INDICATOR_TAG, ACK_INDICATOR_TAG)
