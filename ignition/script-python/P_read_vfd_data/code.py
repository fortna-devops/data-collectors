################################################################
################################################################
## DESC: For use in recording insights (once ~60secs)
## WARN: Modifying code may cause system to function incorrectly
## Copyright 2021 MHS Global, Inc. All rights reserved. 
################################################################
################################################################
import os
import system
import P_opc_data_api
import P_db_api
import P_system_utils

READ_INDICATOR_TAG = 'diVFD_INSIGHTS_DATA_READ'
ACK_INDICATOR_TAG = 'diVFD_INSIGHTS_DATA_READ_ACK'

SENSOR_PATH = 'stVFD_INSIGHTS'
DATAGROUP_DATA = '.stData'       # TODO PLC calls it stData but it is really astData (an array)
DATAGROUP_INFO = '.stInfo'
DATAGROUP_STATS = '.stStats'
DATAGROUP_FAULTS = '.stFault'
DATAGROUP_DATA_FAILURE = '.diDataFailure'

# example
# stVFD_INSIGHTS.stData.stOutputVoltage.rMean

NUM_DATAGROUP_DATA_INSTANCES = 6	# size of array astData

# The number of measurements (reading) for each tag
# incapsulated in the high level data struct stQM30VT_INSIGHTS
NUM_MEASUREMENTS_IN_DATA_STRUCT = 6

PLC_TIMESTAMP = "liTimeStamp"

# Logger:
Module = 'MHS_VFD'
LOG = system.util.logger(Module)

data_tag_to_metric_map = {
	'stOutputCurrent': {
						'diShoeID': 'shoe_id_op_current_max',	#48
						'diStatus': P_db_api.DB_METRIC_OPERATION_CODE,	#13121
						P_opc_data_api.TAG_TIMESTAMP: P_db_api.DB_METRIC_TIMESTAMP,
						'rMax': 'output_current_max',	#12.9
						'rMean': 'output_current_mean',	#12.73
						'rMin': 'output_current_min',	#12.59
						'rSTD': 'output_current_std',	#0.05
					},
	'stOutputFreq': {
					# stOutputFreq subgroup
					'diShoeID': 'shoe_id_op_freq_max',		#423
					'diStatus': P_db_api.DB_METRIC_OPERATION_CODE,		#3121
					P_opc_data_api.TAG_TIMESTAMP: P_db_api.DB_METRIC_TIMESTAMP,
					'rMax': 'output_freq_max',	#59.92
					'rMean': 'output_freq_mean',
					'rMin': 'output_freq_min',
					'rSTD': 'output_freq_std',
					},
	'stOutputPower':{
					# stOutputPower subgroup
					'diShoeID': 'shoe_id_op_power_max',    ### TODO - this whole block is not in db
					'diStatus': P_db_api.DB_METRIC_OPERATION_CODE,
					P_opc_data_api.TAG_TIMESTAMP: P_db_api.DB_METRIC_TIMESTAMP,
					'rMax': 'output_power_max',		#3.61
					'rMean': 'output_power_mean',
					'rMin': 'output_power_min',		#3.21
					'rSTD': 'output_power_std',
				},
	'stOutputVoltage': {
					# stOutputVoltage subgroup
					'diShoeID': 'shoe_id_op_voltage_max',
					'diStatus': P_db_api.DB_METRIC_OPERATION_CODE,
					P_opc_data_api.TAG_TIMESTAMP: P_db_api.DB_METRIC_TIMESTAMP,
					'rMax': 'output_voltage_max',		#450.73
					'rMean': 'output_voltage_mean',
					'rMin': 'output_voltage_min',		#449.85
					'rSTD': 'output_voltage_std',
				},
	# {
	# 'iStatus': 'drive_status'		# TODO: Not read, typo in plc code notice it's not diStatus
	# }
}

# 12-02-21: all values are 9,999
faults_tag_to_metric_map = {
	'iBusVoltage_Fault1': 'bus_voltage_fault',	# TODO: Manish's code divides by 10 (scale by 10 , max value 600)
	'iElapsedTime_Hr_Fault1': '',
	'iElapsedTime_Min_Fault1': 'elapsed_time_minute_fault',
	'iFaultCode1': 'fault_code',
	'iOutputCurrent_Fault1': 'output_current_fault', # TODO: Manish's code divides by 100 
	'iOutputFreq_Fault1': 'output_freq_fault', # TODO: Manish's code divides by 100
	'iStatus_Fault1': 'status_fault',
	P_opc_data_api.TAG_TIMESTAMP: P_db_api.DB_METRIC_TIMESTAMP,
}

# 12-02-21: all values are -9,999
info_tag_to_metric_map =  {
	'iControlSW': 'control_sw',
	'iMotorNPFLA': 'motor_np_fla',			# TODO: Manish's code divides by 10
	'iMotorNPHertz': 'motor_np_hertz',
	'iMotorNPPower': 'motor_np_power',		# TODO: Manish's code divides by 100
	'iMotorNPRPM': 'motor_np_rpm',
	'iMotorNPVolts': 'motor_np_volts',
	'iMotorOLCurrent': 'motor_ol_current',		# TODO: Manish's code divides by 10
	P_opc_data_api.TAG_TIMESTAMP: P_db_api.DB_METRIC_TIMESTAMP,
}

stats_tag_to_metric_map = {
	'iAnalogInput': 'analog_input',
	'iAveragePower': 'average_power', # TODO: Manish's code divides by 100
	'iControlTemp': 'control_temp',
	'iDriveTemp': 'drive_temp',
	'iElapsedKwh': 'elapsed_kwh',		# TODO: Manish's code divides by 10  (max value 100 , in 0.1 KWh)
	'iElapsedMwh': '',
	'iElapsedRunTimeHr': 'elapsed_runtime_hours',
	'iElapsedTimeHr': '',
	'iElapsedTimeMin': 'elapsed_time_minutes',	# TODO: Manish's code divides by 10  (scaled by 10 , max value 600)
	P_opc_data_api.TAG_TIMESTAMP: P_db_api.DB_METRIC_TIMESTAMP,
}

def  read_datagroup_data(datasource_id, sensor_path):	
	for idx in range(NUM_DATAGROUP_DATA_INSTANCES):
		datagroup_path = sensor_path + DATAGROUP_DATA + '[' + str(idx) + ']'

		# Read diStatus and use that as operational_cod for all the subgroups
#		diStatus_tag_path = datagroup_path + '.diStatus'
#		diStatus_tag_value = P_opc_data_api.singleOPCRead(diStatus_tag_path)

		for key, value in data_tag_to_metric_map.items():
			data_subgroup_paths = [datagroup_path + '.' + key]
			P_system_utils.read_tags(datasource_id,
									 data_subgroup_paths,
									 value)

		
def read_datagroup_info(datasource_id, sensor_path):	
	datagroup_paths = [sensor_path + DATAGROUP_INFO]

	P_system_utils.read_tags(datasource_id,
							datagroup_paths,
							info_tag_to_metric_map)

def read_datagroup_stats(datasource_id, sensor_path):	
	pass

def read_datagroup_faults(datasource_id, sensor_path):	
	pass

def test_function(plc_name):
	plc_path = "[{}]".format(plc_name)				# [PS1-13SR]
	sensor_path = plc_path + SENSOR_PATH			# [PS1-13SR]stVFD_INSIGHTS

	# Read device (sensor) id from PLC 
	device_id = P_opc_data_api.read_device_id(sensor_path)
	
	# Convert device id to DB data_source id
	datasource_id = P_db_api.get_data_source_id(str(device_id))
	if datasource_id is None:
		log_string= '{0}:: Cannot find data_source_id for device_id={1}. Skipping but acking'.format(Module, device_id)
		P_db_api.insert_log_table(Module,'WARNING',log_string)
		LOG.warn(log_string)
		# P_opc_data_api.ackTagCounter(plc_name, read_tag, read_ack_tag)
		return;

	#Function to test
	read_datagroup_info(datasource_id, sensor_path)
	
def process(plc_name):
	plc_path = '[{}]'.format(plc_name)				# [PS1-13SR]
	sensor_path = plc_path + SENSOR_PATH			# [PS1-13SR]stVFD_INSIGHTS

	P_system_utils.read_datagroups(plc_name,
								sensor_path,
								read_datagroup_data,
								read_datagroup_info,
								read_datagroup_stats,
								read_datagroup_faults)

	P_opc_data_api.ackTagCounter(plc_name, READ_INDICATOR_TAG, ACK_INDICATOR_TAG)
