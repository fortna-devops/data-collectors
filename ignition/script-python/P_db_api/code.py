################################################################
################################################################
## DESC: For use in recording insights (once ~60secs)
## WARN: Modifying code may cause system to function incorrectly
## Copyright 2021 MHS Global, Inc. All rights reserved. 
################################################################
################################################################
import os
import system
import sys
import time

Module = "DataCollector_DB_Module"
LOG = system.util.logger(Module)

DB_METRIC_TIMESTAMP = 'timestamp'
DB_METRIC_OPERATION_CODE = 'operation_status'
DB_DEFAULT_OP_VALUE = 0

DB_CONNECTION = 'data_collector_db_connection'


def get_data_source_id(sensor_id):
	'''
	Return the db data_source id whose address column
	matches the sensor_id supplied here.
	Return None if no match.
	'''

	ds_id = None
	if sensor_id is not None:
		query = 'SELECT id FROM equipments.data_sources where address=?'
		value = []
		value.append(str(sensor_id))
		try:	
			results = system.db.runPrepQuery(query, value, DB_CONNECTION)
			if results.getRowCount() > 0:
				ds_id = results[0][0]
			else:
				log_string = 'get_data_source_id() no ds_id for device id={}'.format(sensor_id)
				LOG.error(log_string)
				insert_log_table(Module,'WARNING',log_string)
		except:
			LOG.error("get_data_source_ids id => %s, %s" % (sys.exc_info()[0],sys.exc_info()[1]))
	
	return ds_id

def get_metrics_info(datasource_id):
	'''
	Return info about a metric name in the form below.
	 {
		'temperature': {
						'id': 333,
						'raw_data_table': raw_int_table
						},
		...
	 }
	'''
	query = "SELECT id, name, raw_data_table FROM equipments.metrics where data_source_id=?"
	value = []
	value.append(int(datasource_id))
	metric_dict = {}
	try:
		rows = system.db.runPrepQuery(query, value, DB_CONNECTION)

		for row in rows:
 			metric_dict[row[1]]={
 								'id': row[0],
 								'raw_data_table': row[2]
 								}
	except:
 		LOG.error(" get_metrics_info metrics error => %s, %s" % (sys.exc_info()[0],sys.exc_info()[1]))
	
	return metric_dict
				

def get_operation_code_id(op_value):	
	'''
	Returns id of the row in operationa(al)_codes whose value matches op_value.
	'''
	if op_value is None:
		return 1;
	
	query = "SELECT id FROM equipments.operational_codes where value=?"
	value = []
	value.append(int(op_value))
	op_code_id = 0
	try:	
		results = system.db.runPrepQuery(query,value,DB_CONNECTION)
		if len(results) == 1:
			op_code_id = results[0][0]
		elif len(results) > 1:
			op_code_id =  results[len(results)-1][0]
		else:  #$ len(results) == 0:
			log_string = 'No opcode id found for value {0}'.format(op_value)
 			LOG.error(log_string)
			insert_log_table(Module,'WARNING',log_string)
	except:
 		LOG.error(" get_operation_code_id => %s, %s" % (sys.exc_info()[0],sys.exc_info()[1]))

	if op_code_id == 0:	
		value = []
		value.append(int(DB_DEFAULT_OP_VALUE))
		try:	
			results = system.db.runPrepQuery(query, value, DB_CONNECTION)
			
			if len(results) == 1:
				op_code_id = results[0][0]
			elif len(results) > 1:
				op_code_id =  results[len(results)-1][0]
			else: # len(results)==0
				log_string = 'Default opcode not found in db for {0}'.format(str(op_value))
				LOG.error(log_string)
				insert_log_table(Module,'WARNING',log_string)
	
				log_string = 'op_code ({0}) & default op_code not found in db'.format(str(op_value))
				LOG.error(log_string)
				insert_log_table(Module,'WARNING',log_string)
				op_code_id =  1
		except:
			log_string = 'get_operation_code_id default op id =>{0}:{1}'.format(sys.exc_info()[0], sys.exc_info()[1])
			LOG.error(log_string)
			insert_log_table(Module,'WARNING',log_string)
	
		#LOG.error(" OP value not found in database for device_id=%s " % (str(db_sensor_id),str(op_value)))
	
	return op_code_id

#def insert_row_test(table_name, metric_id, timestamp, value, op_code_id):
#	table_name = 'raw_timestamp_data'
#	query= "INSERT into equipments.{} (metric_id, timestamp, value, created_at, operational_code_id) VALUES(?,{},?,now(),?)"
#	query = query.format(table_name, '(TO_TIMESTAMP(?::double precision / 1000000)::timestamp)')
#	
#	try:
#		results = system.db.runPrepUpdate(query,[int(metric_id), timestamp, value, op_code_id], DB_CONNECTION)
#	except:
#		LOG.error("insert_row => %s, %s" % (sys.exc_info()[0],sys.exc_info()[1]))


def insert_row(table_name, metric_id, timestamp, value, op_code_id):
	'''
	Insert one row into a raw data table.
	'''
	
	if (table_name=='raw_timestamp_data'):
		query= "INSERT into equipments.{} (metric_id, timestamp, value, created_at, operational_code_id) VALUES(?,{},{},now(),?)"
		query = query.format(table_name,'(TO_TIMESTAMP(?::double precision / 1000000)::timestamp)','(TO_TIMESTAMP(?::double precision / 1000000)::timestamp)')
	else:
		query= "INSERT into equipments.{} (metric_id, timestamp, value, created_at, operational_code_id) VALUES(?,{},?,now(),?)"
		query = query.format(table_name, '(TO_TIMESTAMP(?::double precision / 1000000)::timestamp)')

	try:
		results = system.db.runPrepUpdate(query,[int(metric_id), timestamp, value, op_code_id], DB_CONNECTION)
	except:
		LOG.error("insert_row => %s, %s" % (sys.exc_info()[0],sys.exc_info()[1]))

def insert_log_table(module, level, message):
	query = "INSERT into equipments.logs (timestamp, module, level, message) VALUES(now(),?,?,?)"
	# check_str = query.format("logs")

	try:
		results = system.db.runPrepUpdate(query,[module, level, message],DB_CONNECTION)
	except:
		log_string = 'insert_log_table: {0} : {1}'.format(sys.exc_info()[0],sys.exc_info()[1])
		LOG.error(log_string)


def insert_data(datasource_id, measurement_dict):
	'''
	Insert measurements into raw data tables.
	'''
	if not measurement_dict:
		return

	if 	not datasource_id:
		return
		
	metrics_info = get_metrics_info(datasource_id)

	if not(DB_METRIC_TIMESTAMP in measurement_dict):
		log_string = 'insert_data:: timestamp for data_source {} not found'.format(datasource_id)
		LOG.error(log_string)
		return	

	# if no operation(al)_code, use the default code.
	# For example, VFD data do not have operation status!!
	if DB_METRIC_OPERATION_CODE in measurement_dict.keys():
		operation_code = measurement_dict[DB_METRIC_OPERATION_CODE]
		op_code_id = get_operation_code_id(operation_code)
	else:
		op_code_id = get_operation_code_id(DB_DEFAULT_OP_VALUE)

	for metric_name in measurement_dict.keys():
		if (metric_name != DB_METRIC_TIMESTAMP) and (metric_name != DB_METRIC_OPERATION_CODE):
			if metric_name in metrics_info:
				insert_row(metrics_info[metric_name]['raw_data_table'],
						   metrics_info[metric_name]['id'],				# metric id
						   measurement_dict[DB_METRIC_TIMESTAMP],		# timestamp							
						   measurement_dict[metric_name],				# metric_name
						   op_code_id)									# operational_code id
			else:
				# This metric does not exist the db table
				log_string = 'insert_data:: metric={} for data_source={} not found in db'.format(metric_name, datasource_id)
				LOG.error(log_string)
				pass
		else:
			pass	
