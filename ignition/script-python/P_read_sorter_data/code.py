################################################################
################################################################
## DESC: For use in recording insights (once ~60secs)
## WARN: Modifying code may cause system to function incorrectly
## Copyright 2021 MHS Global, Inc. All rights reserved. 
################################################################
################################################################
import os
import system
import P_opc_data_api
import P_db_api
import P_system_utils

READ_INDICATOR_TAG = 'diSORTER_INSIGHTS_DATA_READ'
ACK_INDICATOR_TAG = 'diSORTER_INSIGHTS_DATA_READ_ACK'

SENSOR_PATH = 'stSORTER_INSIGHTS'
DATAGROUP_FAULTS = '.stFault'
DATAGROUP_DATA_FAILURE = '.diDataFailure'
DATAGROUP_STATS = '.stStats'
DATAGROUP_DeviceID = '.diDeviceID'
DATAGROUP_StructID = '.diStructID'

# The number of measurements (reading) for each tag
# incapsulated in the high level data struct stQM30VT_INSIGHTS
NUM_MEASUREMENTS_IN_DATA_STRUCT = 6


# Logger:
Module = 'MHS_SORTER'
LOG = system.util.logger(Module)

# mapping table for PLC tags to DB metrics tag name
faults_tag_to_metric_map = {
	P_opc_data_api.TAG_TIMESTAMP: P_db_api.DB_METRIC_TIMESTAMP,	#0
}

statistics_tag_to_metric_map = {
	P_opc_data_api.TAG_TIMESTAMP: P_db_api.DB_METRIC_TIMESTAMP,
}

stats_tag_to_metric_map = {
	'diAirFlow': 'air_flow',											#0
	'diAirPressure': 'air_pressure',									#0
	'diAirVolumeTotalizer': 'air_volume_total',						#0
	'diAmbientTemperature': 'ambient_temp',							#-999
	'diDriveLeftChainUpperTemperature': 'chain_temp_drive_left_upper',		#0
	'diDriveLeftChainLowerTemperature': 'chain_temp_drive_left_lower',		#0
	'diDriveRightChainUpperTemperature': 'chain_temp_drive_right_upper',	#304
	'diDriveRightChainLowerTemperature': 'chain_temp_drive_right_lower',	#301
	'diFaultTimeMinActive': 'fault_time_active',							#0
	'diFaultTimeMinTotal': 'fault_time_minute_total',						#0
	'diGearboxRPM': 'gearbox_rpm',				#0
	'diMotorRPM': 'motor_rpm',					#0
	'diOdometer': 'odometer',					#101608
	'diOdometer_Oil': 'odometer_oil',			#101608
	'diParcelRate1m': 'parcel_rate_1m',			#0
	'diPowerOnTimeMinTotal': 'poweron_minute_total',	# 1515
	'diRunTimeMinActive': 'runtime_active',			#0
	'diRunTimeMinTotal': 'runtime_minute_total',		#1195
	#'diShoeID': ''				#382 TODO - PLC seems to have real a real value for this
	#'diShoeIDMax': ''			#0
	'diSorterSpeed': 'sorter_speed',	#0
	'diSorterStatus': 'sorter_status',	#1
	'diStopTimeMinActive': 'stoptime_active',	#2
	'diStopTimeMinTotal': 'stoptime_minute_total',	#334
	'diTailLeftChainLowerTemperature': 'chain_temp_tail_left_lower',	#0
	'diTailLeftChainUpperTemperature': 'chain_temp_tail_left_upper',	#0
	'diTailRightChainUpperTemperature': 'chain_temp_tail_right_upper',	#0
	'diTailRightChainLowerTemperature': 'chain_temp_tail_right_lower',	#0
	P_opc_data_api.TAG_TIMESTAMP: P_db_api.DB_METRIC_TIMESTAMP
}

def read_datagroup_data(datasource_id, sensor_path):	
	'''
	Sorter has no Data data group
	'''
	pass


def read_datagroup_info(datasource_id, sensor_path):	
	pass

def read_datagroup_stats(datasource_id, sensor_path):
	datagroup_paths = [sensor_path + DATAGROUP_STATS]
	
	P_system_utils.read_tags(datasource_id,
							 datagroup_paths,
							 stats_tag_to_metric_map)

				 
def read_datagroup_faults(datasource_id, sensor_path):	
	pass


def process(plc_name):
	plc_path = '[{}]'.format(plc_name)				# [PS1-13SR]
	sensor_path = plc_path + SENSOR_PATH			# [PS1-13SR]stSORTER_INSIGHTS
	
	P_system_utils.read_datagroups(plc_name,
									sensor_path,
									read_datagroup_data,
									read_datagroup_info,
									read_datagroup_stats,
									read_datagroup_faults)

	P_opc_data_api.ackTagCounter(plc_name, READ_INDICATOR_TAG, ACK_INDICATOR_TAG)
